import express from 'express';
import Review from './ReviewModel';
import asyncHandler from 'express-async-handler';

const router = express.Router(); // eslint-disable-line

router.post('/', asyncHandler(async (req, res) => {
    // 假设 req.body 已经是一个包含所需字段的对象
    const reviewData = {
        author: req.body.author,
        review: req.body.review,
        rating: req.body.rating,
        movieId: req.body.movieId
    };

    const review = await Review.create(reviewData);
    res.status(201).json(review);
}));


export default router;