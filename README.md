# Assignment 2 - Agile Software Practice.

Name: Yifan Gu

## API endpoints.

+ GET /api/movies/tmdb/movie/:id - Get movie detail
+ GET /api/movies/tmdb/movie/:id/reviews -| Get movie reviews
+ GET /api/movies/tmdb/upcoming |- Get upcoming movie list
+ GET /api/movies/tmdb/genre - Get movie genre  
+ GET /api/movies/tmdb/topRated - Get toprated movie list
+ GET /api/movies/tmdb/currentPopular - Get current popular movie list
+ GET /api/movies/tmdb/movie/:id/images - Get movie images
+ POST /api/riviews - Upload movie review
+ GET /api/people/tmdb/popular - Get popular people list
+ GET /api/people/tmdb/people/:id - Get people detail
+ GET /api/people/tmdb/people/:id/images - Get people images
+ POST /api/users - User login
+ POST /api/users?action=register - User sign up

## Automated Testing.
~~~
Movies endpoint
    ✔ should get upcoming movies with a 200 status code (338ms)
    ✔ should get topRated movies with a 200 status code (130ms)
database connected to movies on ac-8misgeu-shard-00-02.6ulincw.mongodb.net
    ✔ should get currentPopular movies with a 200 status code (242ms)
    Get movies by id
      ✔ should return movie details for a given ID (107ms)
      ✔ should return movie images for a given ID (111ms)
      ✔ should return movie reviews for a given ID (127ms)

  People end point
    ✔ should return people list (133ms)
    Get people by id
      ✔ should return people details for a given ID (119ms)
      ✔ should return people images for a given ID (136ms)

  Reviews endpoint
    ✔ should create a new review (90ms)

  User Registration and Authentication
    ✔ should register and authenticate a user (324ms)
Delete operation result: { acknowledged: true, deletedCount: 1 }


  11 passing (2s)
~~~

## Deployments.

+ https://movies-api-staging-yfg-9dddd6b92081.herokuapp.com/api/movies/tmdb/upcoming
+ https://movies-api-staging-yfg-9dddd6b92081.herokuapp.com/api/movies/tmdb/topRated
+ https://movies-api-staging-yfg-9dddd6b92081.herokuapp.com/api/movies/tmdb/currentPopular
+ https://movies-api-staging-yfg-9dddd6b92081.herokuapp.com/api/movies/tmdb/genre
+ https://movies-api-staging-yfg-9dddd6b92081.herokuapp.com/api/movies/tmdb/movie/590706
+ https://movies-api-staging-yfg-9dddd6b92081.herokuapp.com/api/movies/tmdb/movie/695721/reviews
+ https://movies-api-staging-yfg-9dddd6b92081.herokuapp.com/api/movies/tmdb/movie/590706/images
+ https://movies-api-staging-yfg-9dddd6b92081.herokuapp.com/api/people/tmdb/popular
+ https://movies-api-staging-yfg-9dddd6b92081.herokuapp.com/api/people/tmdb/people/1548301
+ https://movies-api-staging-yfg-9dddd6b92081.herokuapp.com/api/people/tmdb/people/1548301/images

## Independent Learning (if relevant)

https://coveralls.io/gitlab/20095253/agile-assignment2#step1

I learnt about HEROKU Staging Deployment, and generate a report on test code coverage through Istanbul and upload it to Coveralls. I am using coveralls-next language integration, GitHub link: https://github.com/jtwebman/coveralls-next.

## Gitlab link:

https://gitlab.com/20095253/agile-assignment2
