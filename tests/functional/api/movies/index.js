import chai from "chai";
import request from "supertest";
import api from "../../../../index";  // Express API application 

const expect = chai.expect;

const token ='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXIxIiwiaWF0IjoxNzAxMzU0NDY2fQ.syt8z6XoREEHK6dcUPcwKRpjm55WxCIxUojxEeEpyLI';

describe('Movies endpoint', () => {
  it('should get upcoming movies with a 200 status code', async function() {
    this.timeout(10000); 
try{
    const response = await request(api)
      .get('/api/movies/tmdb/upcoming') 
      .set('Authorization', `Bearer ${token}`) 
      .expect('Content-Type', /json/)
      .expect(200);
      
    expect(response.body).to.be.an('object'); 
    expect(response.body).to.have.property('results'); 
    expect(response.body.results).to.be.an('array'); 

    if (response.body.results.length > 0) {
      expect(response.body.results[0]).to.include.all.keys('id', 'title', 'release_date'); 
    }
} catch (error) {
  if (error.response) {
    console.log('Response body:', error.response.text);
  }
  throw error; 
}
  });


  it('should get topRated movies with a 200 status code', async function() {
    this.timeout(10000); 

    const response = await request(api)
      .get('/api/movies/tmdb/topRated') 
      .set('Authorization', `Bearer ${token}`) 
      .expect('Content-Type', /json/)
      .expect(200);
    
    expect(response.body).to.be.an('object'); 
    expect(response.body).to.have.property('results'); 
    expect(response.body.results).to.be.an('array'); 

    if (response.body.results.length > 0) {
      expect(response.body.results[0]).to.include.all.keys('id', 'title', 'release_date');
    }

  });
  
  it('should get currentPopular movies with a 200 status code', async function() {
    this.timeout(10000); 

    const response = await request(api)
      .get('/api/movies/tmdb/currentPopular') 
      .set('Authorization', `Bearer ${token}`) 
      .expect('Content-Type', /json/)
      .expect(200);
    
    expect(response.body).to.be.an('object'); 
    expect(response.body).to.have.property('results'); 
    expect(response.body.results).to.be.an('array'); 

    if (response.body.results.length > 0) {
      expect(response.body.results[0]).to.include.all.keys('id', 'title', 'release_date');
    }

  });
  describe('Get movies by id', () => {
    it('should return movie details for a given ID', async function() {
      const testMovieId = '590706'; 
  
      const response = await request(api)
        .get(`/api/movies/tmdb/movie/${testMovieId}`)
        .set('Authorization', `Bearer ${token}`) 
        .expect('Content-Type', /json/)
        .expect(200);
      
      expect(response.body).to.be.an('object');
  
      expect(response.body).to.include.all.keys('title');
  
    });
  
    it('should return movie images for a given ID', async function() {
      const testMovieId = '590706'; 
  
      const response = await request(api)
        .get(`/api/movies/tmdb/movie/${testMovieId}/images`)
        .set('Authorization', `Bearer ${token}`) 
        .expect('Content-Type', /json/)
        .expect(200);
      
      expect(response.body).to.be.an('object');
  
      expect(response.body).to.include.all.keys('backdrops');
  
    });
    it('should return movie reviews for a given ID', async function() {
      const testMovieId = '695721';
  
      const response = await request(api)
        .get(`/api/movies/tmdb/movie/${testMovieId}/reviews`)
        .set('Authorization', `Bearer ${token}`) 
        .expect('Content-Type', /json/)
        .expect(200);
      
      expect(response.body).to.be.an('array');
  
      if (response.body.length > 0) {
        expect(response.body[0]).to.include.all.keys('author', 'author_details', 'content', 'created_at', 'id', 'updated_at', 'url');
  
        expect(response.body[0].author_details).to.be.an('object');
        expect(response.body[0].author_details).to.include.all.keys('name', 'username', 'avatar_path', 'rating');
      }
  
  });
  });
});