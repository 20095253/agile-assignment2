import chai from "chai";
import request from "supertest";
import api from "../../../../index";
import User from "../../../../api/users/userModel";

const expect = chai.expect;

describe('User Registration and Authentication', () => {
    const userCredentials = {
        username: "testuser",
        password: "Password123#"
      };
  it('should register and authenticate a user', async function() {
    // Sign up
    let response = await request(api)
      .post('/api/users?action=register')
      .send(userCredentials)
      .expect('Content-Type', /json/)
      .expect(201);
    
    expect(response.body).to.have.property('success', true);
    expect(response.body).to.have.property('msg', 'User successfully created.');


    // Login
    response = await request(api)
      .post('/api/users')
      .send(userCredentials)
      .expect('Content-Type', /json/)
      .expect(200);

    expect(response.body).to.have.property('success', true);
    expect(response.body).to.have.property('token');
    const token = response.body.token;
    expect(token).to.be.a('string');
  });

  // Delete user
  after(async () => {
    try {
      const result = await User.deleteOne({ username: userCredentials.username });
      console.log('Delete operation result:', result);
    } catch (error) {
      console.error('Failed to delete test user:', error);
    }
  });
});