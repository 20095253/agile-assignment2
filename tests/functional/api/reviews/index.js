import chai from "chai";
import request from "supertest";
import api from "../../../../index";  // Express API application 
import Review from "../../../../api/reviews/ReviewModel";

const expect = chai.expect;

//const token ='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXIxIiwiaWF0IjoxNzAxMzU0NDY2fQ.syt8z6XoREEHK6dcUPcwKRpjm55WxCIxUojxEeEpyLI';
//const testMovieId = '695721';

describe('Reviews endpoint', () => {
    let createdReviewId;
    it('should create a new review', async function() {
      const reviewData = {
        author: "Test Author",
        review: "This is a test review01.",
        rating: 2,
        movieId: "12345"
      };
  
      const response = await request(api)
        .post('/api/reviews')
        .send(reviewData)
        .expect('Content-Type', /json/)
        .expect(201);
        createdReviewId = response.body._id;
      

      expect(response.body).to.include.keys('author', 'review', 'rating', 'movieId');
      expect(response.body.author).to.equal(reviewData.author);
      expect(response.body.review).to.equal(reviewData.review);
    });

    afterEach(async () => {
        // Delete test review
        if (createdReviewId) {
          await Review.deleteOne({ _id: createdReviewId });
        }
      });
  });
