import chai from "chai";
import request from "supertest";
import api from "../../../../index";  // Express API application 

const expect = chai.expect;

const token ='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXIxIiwiaWF0IjoxNzAxMzU0NDY2fQ.syt8z6XoREEHK6dcUPcwKRpjm55WxCIxUojxEeEpyLI';
const testPeopleId ='1548301';
describe('People end point', () => {
    it('should return people list', async function() {
     
      const response = await request(api)
        .get(`/api/people/tmdb/popular`)
        .set('Authorization', `Bearer ${token}`) 
        .expect('Content-Type', /json/)
        .expect(200);
      
      expect(response.body).to.be.an('object');
  
      expect(response.body).to.include.all.keys('page');
  
    });

    describe('Get people by id', () => {
        it('should return people details for a given ID', async function() {
      
          const response = await request(api)
            .get(`/api/people/tmdb/people/${testPeopleId}`)
            .set('Authorization', `Bearer ${token}`) 
            .expect('Content-Type', /json/)
            .expect(200);
          
          expect(response.body).to.be.an('object');
      
          expect(response.body).to.include.all.keys('gender');
      
        });
        it('should return people images for a given ID', async function() {
        
            const response = await request(api)
              .get(`/api/people/tmdb/people/${testPeopleId}/images`)
              .set('Authorization', `Bearer ${token}`) 
              .expect('Content-Type', /json/)
              .expect(200);
            
            expect(response.body).to.be.an('object');
        
            expect(response.body).to.include.all.keys('id',"profiles");
        
          });
      });

  });